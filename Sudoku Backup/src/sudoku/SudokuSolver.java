package sudoku;

import java.util.HashSet;
import java.util.Set;

public class SudokuSolver implements SudokuInterface
{
	private int[][] table;

	
	public SudokuSolver()
	{
		this.table = new int[9][9];
		for(int i = 0; i < table.length; i++)
		{
			for(int ii = 0; ii < table.length; ii++)
			{
				table[i][ii] = 0;
			}
		}
	}
	
	@Override
	public boolean solve() 
	{
		//Start in top left corner
		return solve(0,0);
	}
	//Recursive method to solve sudoku
	private boolean solve(int r, int c)
	{
		int newR, newC;
		//Base case, if looked through whole table return true.
		if(r == table.length)
		{
			return true;
		}
		//If not on last column move to next column.
		if(c < table.length - 1)
		{
			newC = c + 1;
			newR = r;
		}
		//Else restart at next row with c = 0
		else
		{
			newC = 0;
			newR = r + 1;
		}
		//See if value is empty
		if(table[r][c] == 0)
		{
			for(int i = 1; i <= table.length; i++)
			{
				if(checkValid(r, c, i))
				{
					//Try inserting i
					table[r][c] = i;
					//Keep solving, return true if all pass
					if(solve(newR, newC))
					{
						return true;
					}
					//Else set value to 0 and goto next value in loop
					table[r][c] = 0;
				}
			}
			return false;
		}
		//If value is not empty, check for validity and continue solving.
		return isValid() && solve(newR, newC);
	}
	@Override
	public void add(int row, int col, int digit) 
	{
		checkArgException(row, col, digit);
		table[row][col] = digit;
	}

	@Override
	public void remove(int row, int col) 
	{
		checkArgException(row, col);
		table[row][col] = 0;
	}

	@Override
	public int get(int row, int col) 
	{
		checkArgException(row, col);
		return table[row][col];
	}

	@Override
	public boolean isValid()
	{
		for(int row = 0; row < table.length; row++)
		{
			for(int col = 0; col < table.length; col++)
			{
				return validRow(row) && validCol(col) && validGrid(row, col);
			}
		}
		return false;
	}
	/**
	 * Checks if specified row follows the game rules.
	 * @param row Row to check
	 * @return True if all values in row are valid, else false
	 */
	private boolean validRow(int row)
	{
		Set<Integer> temp = new HashSet<>();
		for(int i = 0; i < table.length; i++)
		{
			if(!temp.add(table[row][i]) && table[row][i] != 0)
			{
				return false;
			}
		}
		return true;
	}
	/**
	 * Checks if specified column follows the game rules.
	 * @param col Column to check
	 * @return True if all values in column are valid, else false
	 */
	private boolean validCol(int col)
	{
		Set<Integer> temp = new HashSet<>();
		for(int i = 0; i < table.length; i++)
		{
			if(!temp.add(table[i][col]) && table[i][col] != 0)
			{
				return false;
			}
		}
		return true;
	}
	/**
	 * Checks if grid containing the point (row, col) follows the game rules
	 * @param row Row of point contained in the grid
	 * @param col Column of point contained in grid
	 * @return True if all values in grid are valid, else false
	 */
	private boolean validGrid(int row, int col)
	{
		Set<Integer> temp = new HashSet<>();
		int startRow = (int)Math.floor(row / 3) * 3;
		int startCol = (int)Math.floor(col / 3) * 3;
		for(int r = startRow; r < startRow + 3; r++)
		{
			for(int c = startCol; c < startCol + 3; c++)
			{
				if(!temp.add(table[r][c]) && table[r][c] != 0)
				{
					return false;
				}
			}
		}
		return true;
	}
	/**
	 *  Private method to see if a given value can be inserted at a given position.
	 * @param row Row to check
	 * @param col Column to check
	 * @param digit Value to be inserted
	 * @return True if digit can be placed at (row, col) according to the game rules, else false.
	 */
	private boolean checkValid(int row, int col, int digit)
	{
		return insertRow(row, digit) && insertCol(col, digit) && insertGrid(row, col, digit);
	}
	/**
	 * Checks if a row contains a duplicate of a given value.
	 * @param row Row to check for duplicates
	 * @param digit Value to look through row for
	 * @return True if the value can be placed on the row according to the game rules, else false
	 */
	private boolean insertRow(int row, int digit)
	{
		Set<Integer> temp = new HashSet<>();
		//Iterate over row and add to set.
		for(int i = 0; i < table.length; i++)
		{
			temp.add(table[row][i]);
		}
		//Return false if digit was added while iterating.
		return temp.contains(digit) && digit != 0 ? false : true;
	}
	/**
	 * Checks if a column contains duplicate of a given value.
	 * @param col Column to check for duplicates
	 * @param digit Value to look through column for
	 * @return True if the value can be placed on the column according to the game rules, else false
	 */
	private boolean insertCol(int col, int digit)
	{
		Set<Integer> temp = new HashSet<>();
		//Same logic as insertRow().
		for(int i = 0; i < table.length; i++)
		{
			temp.add(table[i][col]);
		}
		
		return temp.contains(digit) && digit != 0 ? false : true;
	}
	/**
	 * Checks if a grid containing (row, col) contains a duplicate of a given value
	 * @param row Row of the value contained in the grid
	 * @param col Column of the value contained in the grid
	 * @param digit Value to look for duplicates of
	 * @return True if the value could be inserted into the grid according to the game rules, else false
	 */
	private boolean insertGrid(int row, int col, int digit)
	{
		//Values use the same assignment as validGrid().
		int startRow = (int)Math.floor(row / 3) * 3;
		int startCol = (int)Math.floor(col / 3) * 3;
		Set<Integer> temp = new HashSet<>();
		for(int r = startRow; r < startRow + 3; r++)
		{
			for(int c = startCol; c < startCol + 3; c++)
			{
				temp.add(table[r][c]);
			}
		}
		return temp.contains(digit) && digit != 0 ? false : true;
	}
	
	@Override
	public void clear() 
	{
		for(int[] row : table)
		{
			for(int i = 0; i < row.length; i++)
			{
				row[i] = 0;
			}
		}
	}

	@Override
	public void setTable(int[ ][ ] m) 
	{
		if(m.length != 9 || m[0].length != 9)
		{
			throw new IllegalArgumentException();
		}
		for(int row = 0; row < m.length; row++)
		{
			for(int col = 0; col < m.length; col++)
			{
				checkArgException(m[row][col]);
				table[row][col] = m[row][col];
			}
		}
	}

	@Override
	public int[][] getTable() 
	{
		return table;
	}
	/**
	 * Private method to check if methods should throw IllegalArgumentException.
	 * @param args Arguments to be passed into the test
	 */
	private void checkArgException(int... args)
	{
		for(int i : args)
		{
			//Check if i is inside the allowed value
			if(i > 9 || i < 0)
			{
				throw new IllegalArgumentException();
			}
		}
	}
}
