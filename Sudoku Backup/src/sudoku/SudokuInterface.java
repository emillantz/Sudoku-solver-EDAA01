package sudoku;

public interface SudokuInterface 
{
	/**
	 * Attempts to solve the sudoku using recursive backtracking.
	 * @return true if sudoku was solved successfully, else false.
	 */
	boolean solve();

	/**
	 * Puts digit in the box row, col.
	 * 
	 * @param row The row
	 * @param col The column
	 * @param digit The digit to insert in box row, col
	 * @throws IllegalArgumentException if row, col or digit is outside the range [0..9]
	 */
	void add(int row, int col, int digit);

	/**
	 * Removes digit from the box row, col.
	 * @param row The row
	 * @param col The column
	 * @throws IllegalArgumentExpression if row or col is outside the range [0..9]
	 */
	void remove(int row, int col);

	/**
	 * Returns value of the digit in the box row, col
	 * @param row The row
	 * @param col The column
	 * @return Value of the digit contained in the box
	 * @throws IllegalArgumentExpression if row or col is outside the range [0..9]
	 */
	int get(int row, int col);

	/**
	 * Checks that all filled in digits follows the the sudoku rules.
	 * @return True if all values are valid, else false
	 */
	boolean isValid();

	/**
	 * Clears the board of all digits
	 */
	void clear();

	/**
	 * Fills the grid with the digits in m. The digit 0 represents an empty box.
	 * 
	 * @param m the matrix with the digits to insert
	 * @throws IllegalArgumentException if m has the wrong dimensions or contains
	 *                                  values outside the range [0..9]
	 */
	void setTable(int[][] m);

	/**
	 *  Returns the table of the sudoku as an integer matrix.
	 * @return Matrix of the sudoku
	 */
	int[][] getTable();
}