package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sudoku.SudokuInterface;
import sudoku.SudokuSolver;

class SolveTests 
{
	SudokuInterface s;
	//Solvabe sudoku taken from fig. 1 of the manual
	private static final int[ ][ ] fig1 = { {0, 0, 8, 0, 0, 9, 0, 6, 2}, {0, 0, 0, 0, 0, 0, 0, 0, 5}, 
			{1, 0, 2, 5, 0, 0, 0, 0, 0}, {0, 0, 0, 2, 1, 0, 0, 9, 0}, {0, 5, 0, 0, 0, 0, 6, 0, 0}, 
			{6, 0, 0, 0, 0, 0, 0, 2, 8}, {4, 1, 0, 6, 0, 8, 0, 0 ,0}, {8, 6, 0, 0, 3, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 4, 0, 0} };
	@BeforeEach
	void setUp() throws Exception 
	{
		s = new SudokuSolver();
	}

	@AfterEach
	void tearDown() throws Exception 
	{
		s = null;
	}

	@Test
	void testSolveEmpty() 
	{
		//See that solver returns true when trying to solve empty sudoku.
		assertTrue (s.solve());
	}
	@Test
	void testSolveEasy()
	{
		//See that solver returns true when trying to solve sudoku from the manual.
		s.setTable(fig1);
		assertTrue (s.solve());
		assertTrue(true);
	}
	@Test
	void testGetAndClear()
	{
		//Set table as figure from the manual and check that values have been properly inserted.
		s.setTable(fig1);
		assertEquals(s.get(8, 6), 4);
		//Clear and check that the same position is now empty (0).
		s.clear();
		assertEquals(s.get(8, 6), 0);
	}
	@Test
	void testAddAndRemove()
	{
		s.add(4, 7, 3);
		assertEquals(s.get(4, 7), 3);
		s.remove(4, 7);
		assertEquals(s.get(4, 7), 0);
	}
	@Test
	void testValid()
	{
		//Check that sudoku from manual is valid.
		s.setTable(fig1);
		assertTrue (s.isValid());
		s.clear();
		//Add 5 twice to the upper left grid and check for invalidity.
		s.add(0, 0, 5);
		s.add(2, 2, 5);
		assertFalse(s.isValid());
	}
	@Test
	void testGetTable()
	{
		s.setTable(fig1);
		for(int row = 0; row < s.getTable().length; row++)
		{
			for(int col = 0; col < s.getTable().length; col++)
			{
				assertEquals(s.getTable()[row][col], fig1[row][col]);
			}
		}
	}
	@Test
	void testThrowErrors()
	{
		//Try adding outside the given intervals.
		assertThrows(IllegalArgumentException.class, () -> s.add(6, 9, 69));
		assertThrows(IllegalArgumentException.class, () -> s.remove(6, 12));
		assertThrows(IllegalArgumentException.class, () -> s.get(6, 69));
		//Try setting table to a matrix with incorrect dimensions.
		int[][] m1 = new int[2][3];
		assertThrows(IllegalArgumentException.class, () -> s.setTable(m1));
		int[][] m2 = new int[3][3];
		//Try setting table to a matrix with values outside the given intervals.
		m2[0][2] = 17;
		assertThrows(IllegalArgumentException.class, () -> s.setTable(m2));
	}
}
