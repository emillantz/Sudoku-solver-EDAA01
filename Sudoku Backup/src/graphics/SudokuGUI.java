package graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;

import sudoku.SudokuInterface;

public class SudokuGUI 
{
	private SudokuInterface s;
	private JPanel game;
	private JTextField[][] f;
	
	public SudokuGUI(SudokuInterface s)
	{
		this.s = s;
		f = new JTextField[9][9];
		SwingUtilities.invokeLater(() -> createWindow("Sudoku", 600, 600));
	}
	/**
	 * Creates the window and adds methods such as JButtons and panes.
	 * @param label Label of the window
	 * @param width Width of the window
	 * @param height Height of the window
	 */
	private void createWindow(String label, int width, int height)
	{
		JFrame window = new JFrame(label);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = window.getContentPane();
		game = new JPanel();
		GridLayout grid = new GridLayout(9, 9);
		game.setLayout(grid);
		JButton solve = new JButton("Solve");
		JButton clear = new JButton("Clear");
		//Listener on solve-button attempts to solve the sudoku.
		solve.addActionListener((e) -> 
		{
			//If it can be solved, display the solution and a message.
			if(s.solve() == true) 
			{
				JOptionPane.showMessageDialog(pane, "The solver could solve the sudoku");
				update();
			}
			//If it cannot be solved, display a message notifying the user.
			else
			{
				JOptionPane.showMessageDialog(pane, "The sudoku could not be solved");
			}
		});
		//Listener on clear-button clears the table.
		clear.addActionListener((e) ->
		{
			s.clear();
			update();
			JOptionPane.showMessageDialog(pane, "The sudoku was cleared");
		});
		
		JPanel buttons = new JPanel();
		buttons.add(clear, BorderLayout.SOUTH);
		buttons.add(solve, BorderLayout.SOUTH);
		
		pane.add(game, BorderLayout.CENTER);
		pane.add(buttons, BorderLayout.SOUTH);
		
		build();
		window.pack();
		window.setVisible(true);
	}
	/**
	 * Creates the sudoku as JTextField elements
	 */
	private void update()
	{
		for(int row = 0; row < 9; row++)
		{
			for(int col = 0; col < 9; col++)
			{
				//Set 0 to empty string so board is not full of zeroes.
				String value = s.get(row, col) > 0 ? Integer.toString(s.get(row, col)) : "";
				f[row][col].setText(value);
			}
		}
	}
	
	private void build()
	{
		for(int row = 0; row < 9; row++)
		{
			for(int col = 0; col < 9; col++)
			{
				JTextField temp = new JTextField();
				setFields(row, col, temp);
				f[row][col] = temp;
			}
		}
	}
	/**
	 * Sets parameters for Specified tf
	 * @param row Row containing the textfield
	 * @param col Column containing the textfield	
	 * @param tf Textfield contained in (row, col)
	 */
	private void setFields(int row, int col, JTextField tf)
	{
		setBackground(row, col, tf);
		
		Dimension dim = new Dimension(600 / 9, 600 / 9);
		Font font = tf.getFont().deriveFont(Font.PLAIN, 600 / 10);
		
		tf.setFont(font);
		tf.setPreferredSize(dim);
		tf.setHorizontalAlignment(JTextField.CENTER);
		
		game.add(tf);
		tf.addFocusListener(new FocusListener()
		{
			//Controls behaviour when TextField is in focus
			@Override
			public void focusGained(FocusEvent arg0) 
			{
				tf.setBackground(Color.gray);
			}
			
			//Controls behaviour when TextField is deselected.
			@Override
			public void focusLost(FocusEvent arg0) 
			{
				setBackground(row, col, tf);
				try
				{
					int n = Integer.parseInt(tf.getText());
					if(n == 0)
					{
						tf.setText("");
					}
					s.add(row, col, n);				
				}
				catch(Exception e)
				{
					s.add(row, col, 0);
					tf.setText("");
				}
			}		
		});
	}
	/**
	 * Assigns a color to the textfield contained in (row, col) based on a 3x3 color pattern
	 * @param row Row of the textfield
	 * @param col Column of the textfield
	 * @param tf The textfield that will change color
	 */
	
	private void setBackground(int row, int col, JTextField tf)
	{
		int gridRow = (int)Math.floor(row / 3);
		int gridCol = (int)Math.floor(col / 3);
		if(gridRow % 2 == gridCol % 2)
		{
			tf.setBackground(Color.white);
		}
		else
		{
			tf.setBackground(Color.lightGray);
		}
	}
}
//Jag hatar Swing