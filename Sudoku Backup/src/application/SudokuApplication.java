package application;

import graphics.SudokuGUI;
import sudoku.SudokuInterface;
import sudoku.SudokuSolver;

public class SudokuApplication 
{
	public static void main(String[] args) 
	{
		SudokuInterface s = new SudokuSolver();
		new SudokuGUI(s);
	}
}
